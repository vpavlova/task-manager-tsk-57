package ru.vpavlova.tm.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.vpavlova.tm.api.service.model.IGraphService;
import ru.vpavlova.tm.entity.AbstractGraphEntity;

public abstract class AbstractGraphService<E extends AbstractGraphEntity> implements IGraphService<E> {

    @Autowired
    protected ApplicationContext context;

}
