package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.repository.dto.IProjectRepository;
import ru.vpavlova.tm.api.repository.dto.ITaskRepository;
import ru.vpavlova.tm.api.service.dto.IProjectTaskService;
import ru.vpavlova.tm.dto.Task;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;

import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @Autowired
    protected ApplicationContext context;

    @NotNull
    public ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            taskRepository.begin();
            projectRepository.begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIdAndUserId(userId, projectId);
            taskRepository.commit();
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            projectRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
