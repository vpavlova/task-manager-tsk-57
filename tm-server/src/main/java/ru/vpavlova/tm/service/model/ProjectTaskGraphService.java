package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.repository.model.IProjectGraphRepository;
import ru.vpavlova.tm.api.repository.model.ITaskGraphRepository;
import ru.vpavlova.tm.api.service.model.IProjectTaskGraphService;
import ru.vpavlova.tm.entity.TaskGraph;
import ru.vpavlova.tm.exception.empty.EmptyIdException;

import java.util.List;

@Service
public final class ProjectTaskGraphService implements IProjectTaskGraphService {

    @Autowired
    protected ApplicationContext context;

    @NotNull
    public ITaskGraphRepository getTaskRepository() {
        return context.getBean(ITaskGraphRepository.class);
    }

    @NotNull
    public IProjectGraphRepository getProjectRepository() {
        return context.getBean(IProjectGraphRepository.class);
    }

    @NotNull
    @Override
    public List<TaskGraph> findAllTaskByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.unbindTaskFromProjectId(userId, taskId);
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIdAndUserId(userId, projectId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
