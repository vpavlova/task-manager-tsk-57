package ru.vpavlova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.repository.dto.ITaskRepository;
import ru.vpavlova.tm.api.service.dto.ITaskService;
import ru.vpavlova.tm.dto.Task;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyDescriptionException;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.UserNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;

import java.util.List;
import java.util.Optional;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    public ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.add(task);
            taskRepository.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@Nullable final Task task) {
        if (task == null) throw new ObjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.add(task);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Task> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            entities.forEach(taskRepository::add);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.clear();
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Task entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneById(entity.getId());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findAll();
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findById(id);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneById(id);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(
            @Nullable final String userId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.clearByUserId(userId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findAllByUserId(userId);
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findOneByIdAndUserId(userId, id);
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findOneByIndex(userId, index);
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<Task> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findOneByName(userId, name);
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(
            @Nullable final String userId, @Nullable final Task entity
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneById(entity.getId());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneByIdAndUserId(userId, id);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            @NotNull Optional<Task> task = taskRepository.findOneByIndex(userId, index);
            if (!task.isPresent()) throw new UserNotFoundException();
            taskRepository.removeOneByIdAndUserId(userId, task.get().getId());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneByName(userId, name);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable String userId, @Nullable String sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            return taskRepository.findAll();
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) return;
        @NotNull final Optional<Task> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Task> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<Task> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskRepository taskRepository = getRepository();
        try {
            taskRepository.begin();
            taskRepository.update(entity.get());
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}

