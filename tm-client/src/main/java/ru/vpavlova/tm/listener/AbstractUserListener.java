package ru.vpavlova.tm.listener;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vpavlova.tm.endpoint.AdminEndpoint;
import ru.vpavlova.tm.endpoint.SessionEndpoint;
import ru.vpavlova.tm.endpoint.UserEndpoint;

public abstract class AbstractUserListener extends AbstractListener {

    @Nullable
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    @Autowired
    public AdminEndpoint adminEndpoint;

    @Nullable
    @Autowired
    protected SessionEndpoint sessionEndpoint;

}

